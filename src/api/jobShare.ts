import { type ApiCommon, type ApiCommonData, type ApiError, type ApiSuccess, type Flatten, request } from '../utils.js';

export type JobShareVisibility = 'private' | 'organisation' | 'public';

export type JobShare = {
  id: number;
  jobId: number;
  visibility: JobShareVisibility;
  hashKey: string;
  apiUrl: string;
  webUrl: string;
};

export type ApiJobShare = {
  data: {
    id: number;                 // The id of the job share (sortable)
    job_id: number;             // The id of the job related to this share
    visibility: string;         // The visibility of the job share (searchable, sortable)
    hash_key: string;           // The hash key of the job share
    apiUrl: string;             // TODO: not present in API specification!
    webUrl: string;             // TODO: not present in API specification!
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiJobShareData = Flatten<Exclude<ApiJobShare, ApiError>['data']>;

export type ApiJobShareArray = {
  data: ApiJobShareData[];
} & Omit<ApiSuccess, 'data'> | ApiError;

export async function deleteJobShare(jobShareId: number): Promise<Record<string, never>> {
  const path = `/v1/jobs/shares/${jobShareId}`;

  return request<ApiCommon, Record<string, never>>(path, null, null, { method: 'DELETE' });
}
