import { type ApiLayerData, type Layer, layerRevivers } from './layer.js';
import { apiHost, authenticate, token } from '../oauth.js';
import type { ApiMapstyleSetData } from './mapstyleSet.js';
import type { ApiLanguageData } from './language.js';
import {
  APIError,
  type ApiCommon,
  type ApiCommonData,
  type ApiError,
  type ApiSuccess,
  type Flatten,
  HTTPError,
  NetworkError,
  type Revivers,
  defaultListHeader,
  deletedNoneParam,
  lastJobRevision,
  request,
} from '../utils.js';

export type FileFormat =
  | 'png'
  | 'jpg'
  | 'tiff'
  | 'svg'
  | 'pdf'
  | 'web'
  | 'web_download'
  | 'webm'
  | 'mp4'
  | 'mov'
  | 'eps'
  | 'eps_log'
  | 'png_sequence'
  | 'jpg_sequence'
  | 'mxf';

export type JobRevision = {
  id: number;
  jobId: number;
  revision: number;
  languageCode: string;
  mapstyleSetId: number;
  output: FileFormat;
};

export type ApiJobRevision = {
  data: {
    id: number;                 // The id of the job revision (sortable)
    job_id: number;             // The id of the job related to this revision (searchable, sortable)
    revision: number;           // The revision number (searchable, sortable)
    language_code: string;      // The language code of the revision (searchable, sortable)
    mapstyle_set_id: number;    // The id of the mapstyle set
    archived: boolean;          // Whether the revision has been generated (searchable, sortable)
    output: FileFormat;         // The output file type
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiJobRevisionData = Flatten<Exclude<ApiJobRevision, ApiError>['data']>;

export const jobRevisionRevivers: Revivers<ApiJobRevision, JobRevision> = {
  archived: undefined,
};

export async function createJobRevision(
  jobId: number,
  languageCode: string,
  mapstyleSetId: number,
  layers: number[],
  output: FileFormat,
  jobObject: Record<string, unknown>,
): Promise<JobRevision> {
  const pathname = `/v1/jobs/${jobId}/revisions`;
  const path = `${pathname}?${deletedNoneParam}`;
  const body = {
    language_code: languageCode,
    mapstyle_set_id: mapstyleSetId,
    object: JSON.stringify(jobObject),
    output,
    layers,
  };
  const options = { revivers: jobRevisionRevivers };

  return request<ApiJobRevision, JobRevision>(path, body, null, options);
}

export async function getJobRevision(jobId: number): Promise<JobRevision | undefined> {
  const path = `/v1/jobs/${jobId}/revisions/${lastJobRevision}`;
  const options = { revivers: jobRevisionRevivers };

  return request<ApiJobRevision, JobRevision>(path, null, null, options).catch(() => undefined);
}

export async function getJobRevisionObject<JobObject>(jobId: number): Promise<JobObject> {
  const pathname = `/v1/jobs/${jobId}/revisions/${lastJobRevision}/object`;
  const path = `${pathname}?${deletedNoneParam}`;

  return request<ApiCommon, string>(path).then(JSON.parse) as Promise<JobObject>;
}

export type JobCanBuild = {
  canBuild: boolean;
  paymentSource: string;
  reason: string | null;
};

export type ApiJobCanBuild = {
  data: {
    can_build: boolean;
    payment_source: string;
    reason: string | null;
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

// Also known as `canBuild()`
export async function getJobRevisionBuild(jobId: number): Promise<JobCanBuild> {
  const pathname = `/v1/jobs/${jobId}/revisions/${lastJobRevision}/build`;
  const path = `${pathname}?${deletedNoneParam}`;

  return request<ApiJobCanBuild, JobCanBuild>(path);
}

export type JobRevisionBuild = {
  jobRevisionId: number;
  revision: number;
  status: string;
};

export type ApiJobRevisionBuild = {
  data: {
    job_revision_id: number;    // The id of the job revision
    status: string;             // The status of the job result (searchable, sortable)
    contract_id: number;        // The id of the contract that was active when the map was created
    process_start: string;      // The datetime when the job process started (searchable, sortable)
    process_end: string;        // The datetime when the job process ended (searchable, sortable)
    last_downloaded: string;    // The last time the archive was downloaded (searchable, sortable)
    bought: boolean;            // Checks if the result is bought or not (searchable, sortable)
    revision: number;           // The revision number of the job revision
    job_revision: ApiJobRevisionData;
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiJobRevisionBuildData = Flatten<Exclude<ApiJobRevisionBuild, ApiError>['data']>;

export const jobRevisionBuildRevivers: Revivers<ApiJobRevisionBuild, JobRevisionBuild> = {
  contract_id: undefined,
  process_start: undefined,
  process_end: undefined,
  last_downloaded: undefined,
  bought: undefined,
  job_revision: undefined,
};

export async function registerJobRevisionBuild(
  jobId: number,
  status: string,
  start: Date,
  end: Date,
): Promise<JobRevisionBuild> {
  const pathname = `/v1/jobs/${jobId}/revisions/${lastJobRevision}/build/register`;
  const path = `${pathname}?${deletedNoneParam}`;
  const body = { status, process_start: start, process_end: end };
  const options = { revivers: jobRevisionBuildRevivers };

  return request<ApiJobRevisionBuild, JobRevisionBuild>(path, body, null, options);
}

export async function cloneJobRevision(jobId: number, newTitle?: string): Promise<JobRevision> {
  const pathname = `/v1/jobs/${jobId}/revisions/${lastJobRevision}/clone`;
  const path = `${pathname}?${deletedNoneParam}`;
  const body = { title: newTitle ?? null };
  const options = {
    revivers: {
      ...jobRevisionRevivers,
      layers: undefined,
      language: undefined,
      mapstyle_set: undefined,
    },
  };

  type ApiJobRevisionWithData = {
    data: ApiJobRevisionData & {
      layers: ApiLayerData[];
      language: ApiLanguageData;
      mapstyle_set: ApiMapstyleSetData;
    };
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiJobRevisionWithData, JobRevision>(path, body, null, options);
}

export async function listJobRevisionLayers(jobId: number): Promise<Layer[]> {
  const path = `/v1/jobs/${jobId}/revisions/${lastJobRevision}/layers`;
  const options = { revivers: layerRevivers };

  type ApiLayerArray = {
    data: ApiLayerData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiLayerArray, Layer>(path, null, defaultListHeader, options);
}

export type JobRevisionOutputUrl = { url: string };

export type ApiJobRevisionOutputUrl = {
  data: JobRevisionOutputUrl & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export async function getJobRevisionOutputUrl(jobId: number): Promise<JobRevisionOutputUrl> {
  await createJobRevisionBuild(jobId);

  const path = `/v1/jobs/${jobId}/revisions/${lastJobRevision}/result/output-url`;

  return request<ApiJobRevisionOutputUrl, JobRevisionOutputUrl>(path);
}

export type JobRevisionOutput = { blob: Blob; filename?: string | undefined };

export async function getJobRevisionOutput(jobId: number): Promise<JobRevisionOutput> {
  await createJobRevisionBuild(jobId);

  const href = `${apiHost}/v1/jobs/${jobId}/revisions/${lastJobRevision}/result/output`;
  const headers = { ...(token ? { Authorization: token.toString() } : null) };
  const response = await fetch(href, { headers }).catch((error: Error) => {
    throw new NetworkError(error?.message ?? error);
  });

  if (response.ok) {
    const blob = await response.blob().catch(() => {
      throw new APIError({ success: false, error: { type: 'TypeError', message: 'Malformed Blob response' } });
    });
    const contentDisposition = response.headers.get('Content-Disposition');

    if (contentDisposition) {
      const filenameRegex = /filename\*\s*=\s*UTF-8''(.+)/i;
      const filenameMatch = contentDisposition.match(filenameRegex);

      if (filenameMatch?.[1]) {
        return { blob, filename: filenameMatch[1] };
      }

      const fallbackRegex = /filename\s*=\s*"([^"]+)"/i;
      const fallbackMatch = contentDisposition.match(fallbackRegex);

      if (fallbackMatch?.[1]) {
        return { blob, filename: fallbackMatch[1] };
      }
    }

    return { blob };
  } else {
    // eslint-disable-next-line default-case
    switch (response.status) {
      case 401:
        await authenticate();
        break; // NO-OP
      case 403:
      case 404:
      case 406:
      case 429:
        throw new APIError(
          (await response.json().catch(() => ({
            success: false,
            error: { type: 'HttpException', message: response.statusText },
          }))) as ApiError,
        );
    }

    throw new HTTPError(response);
  }
}

async function createJobRevisionBuild(jobId: number): Promise<string> {
  const pathname = `/v1/jobs/${jobId}/revisions/${lastJobRevision}/build`;
  const path = `${pathname}?${deletedNoneParam}`;

  return request<ApiCommon, string>(path, null, null, { method: 'POST' });
}
