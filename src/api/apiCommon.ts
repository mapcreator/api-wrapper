import { type ApiJobData, type Job, jobRevivers } from './job.js';
import {
  APIMeta,
  type ApiError,
  type ApiSuccess,
  type Revivers,
  defaultListHeader,
  getSearchParams,
  request,
} from '../utils.js';

export interface JobSearchResult {
  pageCount: number;
  data: Job[];
}

export async function listJobs(
  pathname: string,
  title: string,
  page: number,
  searchOptions?: Record<string, unknown>,
): Promise<JobSearchResult> {
  const defaultSearchOptions = {
    sort: '-updated',
    only_downloaded: false,
  };

  const search = { ...defaultSearchOptions, ...searchOptions };

  const query = getSearchParams({
    only_with_revisions: true,
    search: {
      job_type_id: '9',
      ...(title.length > 0 && { title: `~:${title}` }),
    },
    ...search,
  });

  const path = `${pathname}?${query}`;

  const headers = {
    ...defaultListHeader,
    'X-Page': `${page}`,
  };
  const options = {
    revivers: jobRevivers as Revivers<ApiJobArray, Job>,
    withMeta: true,
  };

  type ApiJobArray = {
    data: ApiJobData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  const result = (await request<ApiJobArray, Job>(path, null, headers, options).catch(
    (error: Error) => {
      if (error instanceof APIMeta) {
        const { data, headers, status } = error as APIMeta<ApiJobArray, Job>;

        return { data, headers, status };
      }

      throw error;
    },
  )) as APIMeta<ApiJobArray, Job>;

  return {
    data: result.data,
    pageCount: Number(result.headers.get('X-Paginate-Pages')),
  };
}
