import type { ApiCommonData, ApiError, ApiSuccess, Flatten, Revivers } from '../utils.js';

export type Feature = {
  id: number;
  name: string;
};

export type ApiFeature = {
  data: {
    id: number;                 // The feature id
    name: string;               // The feature name (searchable, sortable)
    description: string;        // The description of the feature (searchable, sortable)
    order?: number;             // TODO: not present in API specification!
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiFeatureData = Flatten<Exclude<ApiFeature, ApiError>['data']>;

export const featureRevivers: Revivers<ApiFeature, Feature> = {
  description: undefined,
  order: undefined,
};
