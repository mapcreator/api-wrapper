import { type ApiFontData, type Font, fontRevivers } from './font.js';
import {
  type ApiCommonData,
  type ApiError,
  type ApiSuccess,
  type Flatten,
  type Revivers,
  defaultListHeader,
  getSearchParams,
  request,
} from '../utils.js';

export type FontFamily = {
  id: number;
  name: string;
};

export type ApiFontFamily = {
  data: {
    id: number;                 // The id of the font family (sortable)
    name: string;               // The name of the font family (searchable, sortable)
    order?: number;             // TODO: not present in API specification!
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiFontFamilyData = Flatten<Exclude<ApiFontFamily, ApiError>['data']>;

export const fontFamilyRevivers: Revivers<ApiFontFamily, FontFamily> = {
  order: undefined,
};

export async function listFontFamilyFonts(fontFamilyId: number): Promise<Font[]> {
  const pathname = `/v1/fonts/families/${fontFamilyId}/items`;
  const query = getSearchParams({ sort: 'order' });
  const path = `${pathname}?${query}`;
  const options = { revivers: fontRevivers };

  type ApiFontArray = {
    data: ApiFontData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiFontArray, Font>(path, null, defaultListHeader, options);
}
