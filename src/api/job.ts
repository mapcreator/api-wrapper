import type { ApiJobShare, JobShare, JobShareVisibility } from './jobShare.js';
import { type JobSearchResult, listJobs } from './apiCommon.js';
import {
  type ApiCommon,
  type ApiCommonData,
  type ApiError,
  type ApiSuccess,
  type Flatten,
  type Revivers,
  deletedNoneParam,
  request,
} from '../utils.js';

export type Job = {
  id: number;
  jobTypeId: number;
  title: string;
  previewPath: string | undefined;
  createdAt: string;
  star: boolean;
  jobFolderId: number | null;
  userEmail: string | undefined;
  jobFolderName: string | null;
  userId: number;
};

export type ApiJob = {
  data: {
    id: number;                 // The job id (sortable)
    job_type_id: number;        // The id of the job type (searchable)
    user_id: number;            // The user id (searchable)
    title: string;              // The title of the job (searchable, sortable)
    description: string | null; // The description of the job (searchable, sortable)
    share_token: string | null; // The token used for sharing this job
    autosave_preview_path: string | null; //  The preview path of the Job
    job_folder_id: number | null; // TODO: not present in API specification!
    job_folder_name?: string | null; // TODO: not present in API specification!
    star: boolean;              // TODO: not present in API specification!
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiJobData = Flatten<Exclude<ApiJob, ApiError>['data']>;

export const jobRevivers: Revivers<ApiJob, Job> = {
  description: undefined,
  share_token: undefined,
  autosave_preview_path: undefined,

  jobTypeId: () => 9,
  jobFolderName: (data: ApiJobData) => data.job_folder_name ?? null,
  createdAt: (data: ApiJobData) => data.created_at as string,
  previewPath: (data: ApiJobData) => data.autosave_preview_path ?? undefined,
};

export async function createJob(title: string): Promise<Job> {
  const path = `/v1/jobs`;
  const body = { title, job_type_id: 9 };
  const options = { revivers: jobRevivers };

  // Technically, the returning `data` will contain only the following fields:
  // Pick<ApiJobData, 'id' | 'job_type_id' | 'user_id' | 'title' | 'created_at' | 'updated_at'>

  return request<ApiJob, Job>(path, body, null, options);
}

export async function getJob(jobId: number): Promise<Job> {
  const path = `/v1/jobs/${jobId}`;
  const options = { revivers: jobRevivers };

  return request<ApiJob, Job>(path, null, null, options);
}

export async function deleteJob(jobId: number): Promise<Record<string, never>> {
  const path = `/v1/jobs/${jobId}`;

  return request<ApiCommon, Record<string, never>>(path, null, null, { method: 'DELETE' });
}

export async function updateJob(jobId: number, newTitle: string): Promise<Record<string, never>> {
  const path = `/v1/jobs/${jobId}`;
  const body = { title: newTitle };

  return request<ApiCommon, Record<string, never>>(path, body, null, { method: 'PATCH' });
}

export async function updateJobFolder(jobId: number, folderId: number | null): Promise<Record<string, never>> {
  const path = `/v1/jobs/${jobId}`;
  const body = { job_folder_id: folderId };

  return request<ApiCommon, Record<string, never>>(path, body, null, { method: 'PATCH' });
}

export async function starJob(jobId: number, star: boolean): Promise<Record<string, never>> {
  const path = `/v1/jobs/${jobId}`;
  const body = { star };

  return request<ApiCommon, Record<string, never>>(path, body, null, { method: 'PATCH' });
}

export async function generateJobShare(
  jobId: number,
  visibility: JobShareVisibility,
): Promise<JobShare> {
  const path = `/v1/jobs/${jobId}/share`;
  const body = { visibility };

  return request<ApiJobShare, JobShare>(path, body);
}

export async function uploadJobPreview(
  jobId: number,
  preview: Blob,
): Promise<Record<string, never>> {
  const pathname = `/v1/jobs/${jobId}/preview`;
  const path = `${pathname}?${deletedNoneParam}`;
  const body = new FormData();

  body.append('preview', preview);

  return request<ApiCommon, Record<string, never>>(path, body);
}

export async function listFeaturedJobs(
  title: string,
  page: number,
  options?: Record<string, unknown>,
): Promise<JobSearchResult> {
  return listJobs(`/v1/jobs/featured`, title, page, options);
}
