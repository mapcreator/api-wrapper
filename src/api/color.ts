import type { ApiCommonData, ApiError, ApiSuccess, Flatten, Revivers } from '../utils.js';

export type Color = {
  id: number;
  hex: string;
};

export type ApiColor = {
  data: {
    id: number;                 // The color id (sortable)
    name: string;               // The name of the color (searchable, sortable)
    hex: string;                // The hex value of the color (searchable, sortable)
    order?: number;             // TODO: not present in API specification!
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiColorData = Flatten<Exclude<ApiColor, ApiError>['data']>;

export const colorRevivers: Revivers<ApiColor, Color> = {
  name: undefined,
  order: undefined,
};
