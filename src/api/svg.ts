import { type ApiCommonData, type ApiError, type ApiSuccess, type Flatten, type Revivers, request } from '../utils.js';

export type Svg = {
  id: number;
  svgSetId: number;
  name: string;
  string: string;
};

export type ApiSvg = {
  data: {
    id: number;                 // The id of the svg (sortable)
    svg_set_id: number;         // The id of the svg_set related to this svg
    name: string;               // The name of the svg (searchable, sortable)
    string: string;             // The XML of the svg
    resize_factor: number;      // The number of the resize for svg (searchable, sortable)
    order: number;              // The order in which this svg appears in the set (sortable)
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiSvgData = Flatten<Exclude<ApiSvg, ApiError>['data']>;

export const svgRevivers: Revivers<ApiSvg, Svg> = {
  resize_factor: undefined,
  order: undefined,
};

export async function getSvg(svgId: number): Promise<Svg> {
  const path = `/v1/svgs/${svgId}`;
  const options = { revivers: svgRevivers };

  return request<ApiSvg, Svg>(path, null, null, options);
}
