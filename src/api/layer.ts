import type { ApiCommonData, ApiError, ApiSuccess, Flatten, Revivers } from '../utils.js';

export type Layer = {
  id: number;
  name: string;
  description: string;
  svg: string | null;
};

export type ApiLayer = {
  data: {
    id: number;                 // The id of the layer (sortable)
    name: string;               // The name of the layer (searchable, sortable)
    path: string;               // The path to the layer
    description: string;        // The description of the layer (searchable, sortable)
    image: string | null;       // The image related to a layer
    svg: string | null;         // The svg related to a layer
    scale_min: number;          // The minimum scale for the layer (searchable, sortable)
    scale_max: number;          // The maximum scale for the layer (searchable, sortable)
    pangaea_ready: boolean;     // Whether a layer is prepared for Pangaea (searchable, sortable)
    svg_set_id: number | null;  // TODO: not present in API specification!
    pivot: unknown;             // TODO: not present in API specification!
    order?: number;             // TODO: not present in API specification!
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiLayerData = Flatten<Exclude<ApiLayer, ApiError>['data']>;

export const layerRevivers: Revivers<ApiLayer, Layer> = {
  path: undefined,
  image: undefined,
  scale_min: undefined,
  scale_max: undefined,
  pangaea_ready: undefined,
  svg_set_id: undefined,
  pivot: undefined,
  order: undefined,
};
