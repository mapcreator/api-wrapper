import type { ApiJobShareArray, JobShare } from './jobShare.js';
import { type JobSearchResult, listJobs } from './apiCommon.js';
import {
  type ApiCommonData,
  type ApiError,
  type ApiSuccess,
  type Flatten,
  type Revivers,
  defaultListHeader,
  getSearchParams,
  myOrganisations,
  request,
} from '../utils.js';

export type Organisation = {
  id: number;
  name: string;
  credits: number;
  settings: string | null;
  vectorToken: string;
  trialExpiresAt: string | undefined;
  exportTemplates: string;
  contractType: string;
  hasContract: boolean;
};

export type ApiOrganisation = {
  data: {
    id: number;                 // The id of the organisation (sortable)
    name: string;               // The name of the organisation (searchable, sortable)
    manager_id: number;         // The id of the manager of the organisation (searchable)
    sales_representative_id: number | null; // The id of the sales representative (searchable)
    credits: number;            // The credits of the organisation (searchable, sortable)
    country: string | null;     // The country where the organisation is from (searchable, sortable)
    address: string | null;     // The address of the organisation (searchable, sortable)
    city: string | null;        // The city of the organisation (searchable, sortable)
    settings: string | null;    // An object with settings for the organisations
    vector_token: string;       // The vector API token of the organisation
    tiles_on_domain: boolean;   // TODO: not present in API specification!
    trial_expires_at: string | null; // The datetime when the user's trial expires
    parent_id: number | null;   // TODO: not present in API specification!
    hatching_pattern_svg_set_id: number | null; // A svg set that overrides the default h-pattern
    template_settings: string | null; // TODO: not present in API specification!
    contractType: string | null; // The name of signed contract
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiOrganisationData = Flatten<Exclude<ApiOrganisation, ApiError>['data']>;

export const organisationRevivers: Revivers<ApiOrganisation, Organisation> = {
  manager_id: undefined,
  sales_representative_id: undefined,
  country: undefined,
  address: undefined,
  city: undefined,
  tiles_on_domain: undefined,
  parent_id: undefined,
  hatching_pattern_svg_set_id: undefined,
  template_settings: undefined,

  trialExpiresAt: (data: ApiOrganisationData) => data.trial_expires_at ?? undefined,
  exportTemplates: (data: ApiOrganisationData) => data.template_settings ?? '{}', // @ts-expect-error TS2551
  hasContract: (data: ApiOrganisationData) => !!(data.contractType ?? data.contract_type ?? data.hasContract),
};

export async function getOrganisation(): Promise<Organisation> {
  const path = `/v1/organisations/${myOrganisations}`;
  const options = { revivers: organisationRevivers };

  return request<ApiOrganisation, Organisation>(path, null, null, options);
}

export async function listOrganisationJobs(
  organisationId: number | typeof myOrganisations,
  title: string,
  shared: boolean,
  page: number,
  options?: Record<string, unknown>,
): Promise<JobSearchResult> {
  const search = { ...options, ...{ hidePrivate: true, shared } };

  return listJobs(`/v1/organisations/${organisationId}/jobs`, title, page, search);
}

export async function listOrganisationShares(jobId: number): Promise<JobShare[]> {
  const pathname = `/v1/organisations/${myOrganisations}/shares`;
  const query = getSearchParams({
    search: {
      job_id: `=:${jobId}`,
    },
  });
  const path = `${pathname}?${query}`;

  return request<ApiJobShareArray, JobShare>(path, null, defaultListHeader);
}
