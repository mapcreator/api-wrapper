import {
  APIMeta,
  type ApiCommonData,
  type ApiError,
  type ApiSuccess,
  type Flatten,
  defaultListHeader,
  ensureArray,
  getSearchParams,
  request,
  toAppType,
} from '../utils.js';
import type { FeatureCollection, Polygon } from 'geojson';
import type { SnakeCase } from 'type-fest';

type FieldName = 'id' | 'name' | 'description' | 'previewJson' | 'previewPath';

const processGroupData = (group: GroupChoropleth): GroupChoropleth => ({
  ...group,
  ...group.nativeParentTranslation && { nativeParentTranslation: toAppType(group.nativeParentTranslation) },
  ...group.nativeRelationTranslation && { nativeRelationTranslation: toAppType(group.nativeRelationTranslation) },
  ...group.englishParentTranslation && { englishParentTranslation: toAppType(group.englishParentTranslation) },
  ...group.englishRelationTranslation && { englishRelationTranslation: toAppType(group.englishRelationTranslation) },
  ...group.parentTranslations && { parentTranslations: ensureArray(group.parentTranslations).map(toAppType) },
  ...group.relationTranslations && { relationTranslations: ensureArray(group.relationTranslations).map(toAppType) },
});

const processPolygonData = (polygon: PolygonChoropleth): PolygonChoropleth => ({
  ...polygon,
  ...polygon.parentGroups && { parentGroups: ensureArray(polygon.parentGroups).map(toAppType).map(processGroupData) },
  ...polygon.nativeTranslation && { nativeTranslation: toAppType(polygon.nativeTranslation) },
  ...polygon.englishTranslation && { englishTranslation: toAppType(polygon.englishTranslation) },
  ...polygon.translations && { translations: ensureArray(polygon.translations).map(toAppType) },
});

export type PartialVectorChoropleth = Pick<VectorChoropleth, FieldName>;

export type VectorChoropleth = {
  id: number;
  name: string;
  description: string;
  previewPath: string | null;
  previewJson: FeatureCollection | null;

  vectorSetUrl: string;
  sourceLayerName: string;

  lngMin: number;
  lngMax: number;
  latMin: number;
  latMax: number;

  keys: string[];
  properties: Array<Record<string, unknown>>;
};

export type ApiVectorChoropleth = {
  data: {
    id: number;
    name: string;
    description: string;
    preview_path: string | null;
    preview_json: FeatureCollection | null;

    vector_set_url: string;
    source_layer_name: string;

    lng_min: number;
    lng_max: number;
    lat_min: number;
    lat_max: number;

    keys: string[];
    properties: Array<Record<string, unknown>>;
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiVectorChoroplethData = Flatten<Exclude<ApiVectorChoropleth, ApiError>['data']>;

export async function listVectorChoropleths(name: string): Promise<PartialVectorChoropleth[]> {
  const pathname = `/v1/choropleths/vector`;
  const query = getSearchParams({ search: { name } });
  const path = `${pathname}?${query}`;

  // Only request first 50 results
  const headers = { ...defaultListHeader };
  const options = { withMeta: true };

  type ApiVectorChoroplethArray = {
    data: Array<Pick<ApiVectorChoroplethData, SnakeCase<FieldName>>>;
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiVectorChoroplethArray, PartialVectorChoropleth>(path, null, headers, options)
    .catch((error: Error) => {
      if (error instanceof APIMeta) {
        return (error as APIMeta<ApiVectorChoroplethArray, PartialVectorChoropleth>).data;
      }

      throw error;
    });
}

export async function getVectorChoropleth(choroplethId: number): Promise<VectorChoropleth> {
  const path = `/v1/choropleths/vector/${choroplethId}`;

  return request<ApiVectorChoropleth, VectorChoropleth>(path);
}

export type SearchBounds = {
  min_lat: number;
  min_lng: number;
  max_lat: number;
  max_lng: number;
};

export type PolygonChoropleth = {
  sml: string;
  id: number;
  vectorSource: string;
  sourceLayer: string;
  featureId: number;
  svgPreview: string;
  previewPath: string;
  allowSingle: boolean;
  properties: Record<string, unknown>;
  boundingBox: Polygon;
  restrictedOwnership: number[] | null;
  linkId: number | null;
  cloning: boolean;
  nativeTranslation: {
    polygonId: number;
    name: string;
  };
  englishTranslation?: {
    polygonId: number;
    name: string;
  };
  translations?: Array<{
    polygonId: number;
    language: string;
    name: string;
  }>;
  parentGroups: GroupChoropleth[];
};

export type ApiPolygonChoropleth = {
  data: {
    sml: string;
    id: number;
    vector_source: string;
    source_layer: string;
    feature_id: number;
    svg_preview: string;
    preview_path: string;
    allow_single: boolean;
    properties: Record<string, unknown>;
    bounding_box: Polygon;
    restricted_ownership: number[] | null;
    link_id: number | null;
    cloning: boolean;
    native_translation: {
      polygon_id: number;
      name: string;
    };
    english_translation?: {
      polygon_id: number;
      name: string;
    };
    translations?: Array<{
      polygon_id: number;
      language: string;
      name: string;
    }>;
    parent_groups: ApiGroupChoroplethData[];
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiPolygonChoroplethData = Flatten<Exclude<ApiPolygonChoropleth, ApiError>['data']>;

export async function listPolygonChoropleths(
  name: string,
  languages: string[],
  searchBounds?: SearchBounds,
  withParentGroups = false,
): Promise<PolygonChoropleth[]> {
  const pathname = `/v1/choropleth/polygons`;
  const query = getSearchParams(
    {
      search: { name, allow_single: true },
      with_parent_groups: withParentGroups,
      languages,
      ...searchBounds,
    },
  );
  const path = `${pathname}?${query}`;

  // Only request first 50 results
  const headers = { ...defaultListHeader };
  const options = { withMeta: true };

  type ApiPolygonChoroplethArray = {
    data: ApiPolygonChoroplethData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiPolygonChoroplethArray, PolygonChoropleth>(path, null, headers, options)
    .catch((error: Error) => {
      if (error instanceof APIMeta) {
        return (error as APIMeta<ApiPolygonChoroplethArray, PolygonChoropleth>).data;
      }

      throw error;
    })
    .then(result => result.map(processPolygonData));
}

export type GroupChoropleth = {
  id: number;
  polygonId: number;
  relationId: number;
  childrenCount: number;
  sml?: string;
  svgPreview: string;
  previewPath: string;
  uniqueProperties: string[];
  boundingBox: Polygon;
  partialProperties: string[];
  restrictedOwnership: number[] | null;
  linkId: number | null;
  cloning: boolean;
  laravelThroughKey?: number;
  nativeParentTranslation: {
    polygonId: number;
    name: string;
    laravelThroughKey: number;
  };
  nativeRelationTranslation: {
    relationId: number;
    name: string;
    laravelThroughKey: number;
  };
  englishParentTranslation?: {
    polygonId: number;
    name: string;
    laravelThroughKey: number;
  };
  englishRelationTranslation?: {
    relationId: number;
    name: string;
    laravelThroughKey: number;
  };
  parentTranslations?: Array<{
    polygonId: number;
    name: string;
    language: string;
    laravelThroughKey: number;
  }>;
  relationTranslations?: Array<{
    relationId: number;
    name: string;
    language: string;
    laravelThroughKey: number;
  }>;
};

export type ApiGroupChoropleth = {
  data: {
    id: number;
    polygon_id: number;
    relation_id: number;
    children_count: number;
    sml?: number;
    svg_preview: string;
    preview_path: string;
    unique_properties: string[];
    bounding_box: Polygon;
    partial_properties: string[];
    restricted_ownership: number[] | null;
    link_id: number | null;
    cloning: boolean;
    laravel_through_key?: number;
    native_parent_translation: {
      polygon_id: number;
      name: string;
      laravel_through_key: number;
    };
    native_relation_translation: {
      relation_id: number;
      name: string;
      laravel_through_key: number;
    };
    english_parent_translation?: {
      polygon_id: number;
      name: string;
      laravel_through_key: number;
    };
    english_relation_translation?: {
      relation_id: number;
      name: string;
      laravel_through_key: number;
    };
    parent_translations?: Array<{
      polygon_id: number;
      name: string;
      language: string;
      laravel_through_key: number;
    }>;
    relation_translations?: Array<{
      relation_id: number;
      name: string;
      language: string;
      laravel_through_key: number;
    }>;
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiGroupChoroplethData = Flatten<Exclude<ApiGroupChoropleth, ApiError>['data']>;

export async function listGroupChoropleths(
  name: string,
  languages: string[],
  searchBounds?: SearchBounds,
): Promise<GroupChoropleth[]> {
  const pathname = `/v1/choropleth/groups`;
  const query = getSearchParams({ search: { name }, languages, ...searchBounds });
  const path = `${pathname}?${query}`;

  // Only request first 50 results
  const headers = { ...defaultListHeader };
  const options = { withMeta: true };

  type ApiGroupChoroplethArray = {
    data: ApiGroupChoroplethData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiGroupChoroplethArray, GroupChoropleth>(path, null, headers, options)
    .catch((error: Error) => {
      if (error instanceof APIMeta) {
        return (error as APIMeta<ApiGroupChoroplethArray, GroupChoropleth>).data;
      }

      throw error;
    })
    .then(result => result.map(processGroupData));
}

export async function listChildrenGroupChoropleths(
  groupId: number,
  languages: string[],
): Promise<PolygonChoropleth[]> {
  const pathname = `/v1/choropleth/groups/${groupId}/children`;
  const query = getSearchParams({ languages });
  const path = `${pathname}?${query}`;

  type ApiPolygonChoroplethArray = {
    data: ApiPolygonChoroplethData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiPolygonChoroplethArray, PolygonChoropleth>(path).then(polygons => polygons.map(processPolygonData));
}
