import { type ApiCommonData, type ApiError, type ApiSuccess, type Flatten, type Revivers, getSearchParams, request } from '../utils.js';

export type Font = {
  id: number;
  fontFamilyId: number;
  name: string;
  label: string;
};

export type ApiFont = {
  data: {
    id: number;                 // The id of the font (sortable)
    font_family_id: number;     // The id of the font family
    name: string;               // The name of the font (searchable, sortable)
    style: string;              // The font style (searchable, sortable)
    stretch: string;            // The font stretch (searchable, sortable)
    weight: number;             // The font weight (searchable, sortable)
    label: string;              // The label of the font (searchable, sortable)
    order?: number;             // TODO: not present in API specification!
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiFontData = Flatten<Exclude<ApiFont, ApiError>['data']>;

type FontSearchOptions = {
  family?: string;
  style?: string;
  weight?: string;
};

export const fontRevivers: Revivers<ApiFont, Font> = {
  style: undefined,
  stretch: undefined,
  weight: undefined,
  order: undefined,
};

export async function getFonts(searchOptions: FontSearchOptions): Promise<Font[] | Font> {
  const pathname = `/v1/fonts/search`;
  const query = getSearchParams(searchOptions);

  const path = `${pathname}?${query}`;

  type ApiFontArray = {
    data: ApiFontData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiFontArray, Font>(path);
}
