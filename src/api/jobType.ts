import type { ApiCommonData, ApiError, ApiSuccess, Flatten, Revivers } from '../utils.js';

export type JobType = {
  id: number;
};

export type ApiJobType = {
  data: {
    id: number;                 // The id of the job type (sortable)
    name: string;               // The name of the job type (searchable, sortable)
    preview: string | null;     // The preview file name of the job type
    description: string | null; // The description of the job type (searchable, sortable)
    components: string | null;  // The components of the job type
    order?: number;             // TODO: not present in API specification!
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiJobTypeData = Flatten<Exclude<ApiJobType, ApiError>['data']>;

export const jobTypeRevivers: Revivers<ApiJobType, JobType> = {
  name: undefined,
  preview: undefined,
  description: undefined,
  components: undefined,
  order: undefined,
};
