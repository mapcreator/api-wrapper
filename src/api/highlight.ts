import type { FeatureCollection } from 'geojson';
import type { SnakeCase } from 'type-fest';
import {
  APIMeta,
  type ApiCommonData,
  type ApiError,
  type ApiSuccess,
  type Flatten,
  defaultListHeader,
  getSearchParams,
  request,
} from '../utils.js';

type FieldName = 'id' | 'name' | 'description' | 'previewPath' | 'previewJson';

export type PartialVectorHighlight = Pick<VectorHighlight, FieldName>;

export type VectorHighlight = {
  id: number;
  name: string;
  description: string;
  previewPath: string | null;
  previewJson: FeatureCollection | null;

  vectorSetUrl: string;
  sourceLayerName: string;

  lngMin: number;
  lngMax: number;
  latMin: number;
  latMax: number;

  keys: string[];
  properties: Record<string, unknown>;
};

export type ApiVectorHighlight = {
  data: {
    id: number;
    name: string;
    description: string;
    preview_path: string | null;
    preview_json: FeatureCollection | null;

    vector_set_url: string;
    source_layer_name: string;

    lng_min: number;
    lng_max: number;
    lat_min: number;
    lat_max: number;

    keys: string[];
    properties: Record<string, unknown>;
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiVectorHighlightData = Flatten<Exclude<ApiVectorHighlight, ApiError>['data']>;

export async function listVectorHighlights(name: string): Promise<PartialVectorHighlight[]> {
  const pathname = `/v1/highlights/vector`;
  const query = getSearchParams({ search: { name } });
  const path = `${pathname}?${query}`;

  // Only request first 50 results
  const headers = { ...defaultListHeader };
  const options = { withMeta: true };

  type ApiVectorHighlightArray = {
    data: Array<Pick<ApiVectorHighlightData, SnakeCase<FieldName>>>;
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiVectorHighlightArray, PartialVectorHighlight>(path, null, headers, options)
    .catch((error: Error) => {
      if (error instanceof APIMeta) {
        return (error as APIMeta<ApiVectorHighlightArray, PartialVectorHighlight>).data;
      }

      throw error;
    });
}

export async function getVectorHighlight(highlightId: number): Promise<VectorHighlight> {
  const path = `/v1/highlights/vector/${highlightId}`;

  return request<ApiVectorHighlight, VectorHighlight>(path);
}
