import type { ApiCommonData, ApiError, ApiSuccess, Flatten, Revivers } from '../utils.js';

export type OutputUnit =
  | 'millimeter'
  | 'centimeter'
  | 'inch'
  | 'pixel'
  | 'pica';

export type Dimension = {
  id: number;
  dimensionSetId: number;
  name: string;
  width: number;
  height: number;
  dpi: number;
  unit: OutputUnit;
};

export type ApiDimension = {
  data: {
    id: number;                 // The dimension id (sortable)
    dimension_set_id: number;   // The id of the dimension set
    name: string;               // The name of dimension (searchable, sortable)
    width: number;              // The width of the dimension (searchable, sortable)
    height: number;             // The height of the dimension (searchable, sortable)
    dpi: number;                // The dpi of the dimension
    unit: string;               // The unit of this dimension
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiDimensionData = Flatten<Exclude<ApiDimension, ApiError>['data']>;

const keyToUnit: { [key: string]: OutputUnit } = {
  MM: 'millimeter',
  CM: 'centimeter',
  IN: 'inch',
  PX: 'pixel',
  PICA: 'pica',
};

export const dimensionRevivers: Revivers<ApiDimension, Dimension> = {
  unit: (data: ApiDimensionData) => keyToUnit[data.unit],
};
