import type { ApiCommonData, ApiError, ApiSuccess, Flatten } from '../utils.js';

export type Language = {
  code: string;
  name: string;
  locale: string;
  interface: boolean;
};

export type ApiLanguage = {
  data: {
    code: string;               // The language code
    name: string;               // The name of the language (searchable, sortable)
    locale: string;             // The locale string for the language (searchable, sortable)
    interface: boolean;         // If this language can be used for user interfaces
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiLanguageData = Flatten<Exclude<ApiLanguage, ApiError>['data']>;
