import { type ApiFontFamilyData, type FontFamily, fontFamilyRevivers } from './fontFamily.js';
import { type ApiSvgSetData, type SvgSet, svgSetRevivers } from './svgSet.js';
import { type ApiLayerData, type Layer, layerRevivers } from './layer.js';
import { type ApiSvgData, type Svg, svgRevivers } from './svg.js';
import { type JobSearchResult, listJobs } from './apiCommon.js';
import type { ApiJobShareArray, JobShare } from './jobShare.js';
import type { RequireAtLeastOne } from 'type-fest';
import {
  type ApiCommon,
  type ApiCommonData,
  type ApiError,
  type ApiSuccess,
  type Flatten,
  type Revivers,
  defaultListHeader,
  deletedNoneParam,
  getSearchParams,
  myUser,
  request,
  toApiType,
} from '../utils.js';

type ApiPermission = {
  id: number;                   // The id of the permission
  name: string;                 // The name of the permission (searchable, sortable)
} & ApiCommonData;

type ApiRole = {
  id: number;
  name: string;
  description: string;
  pivot: {
    model_type: string;
    model_id: number;
    role_id: number;
  };
} & ApiCommonData;

type UnitType = 'metric' | 'imperial' | null;

export type User = {
  id: number;
  languageCode: string;
  interfaceLanguage: string;
  videoNotifications: boolean;
  name: string;
  email: string;
  company: string;
  confirmed: boolean;
  propagated: boolean;
  whatsNewBadge: boolean;
  autosave: boolean;
  unit: UnitType;
  trialExpiresAt: string | undefined;
  stripeId: string | undefined;
  intercomIdentityToken?: string;
  hasPassword?: boolean;
};
/* eslint-disable @typescript-eslint/no-redundant-type-constituents */
export type ApiUser = {
  data: {
    id: number;                 // The id of the user (searchable, sortable)
    organisation_id: number;    // The id of the organisation related to a user (searchable)
    language_code: string;      // The default map language
    interface_language: string; // The default interface language
    hide_messages: boolean;     // If the what's new notifications should be shown
    video_notifications: boolean; // TODO: not present in API specification!
    name: string;               // The name of the user (searchable, sortable)
    email: string;              // The email of the user (searchable, sortable)
    phone: string | null;       // The phone of the user (searchable, sortable)
    profession: string | null;  // The user's profession (searchable, sortable)
    company: string | null;     // The user's company (sortable)
    country: string | null;     // The user's country (sortable)
    city: string | null;        // The user's city (sortable)
    address: string | null;     // The user's address (sortable)
    tips: boolean;              // If the user wishes to see tips
    tags: string[] | null;      // The tags related to a user (searchable, sortable)
    confirmed: boolean;         // Whether the user email is verified
    propagated: boolean;        // Set to 'true' on the first user login (hubspot)
    newsletter_consent: boolean; // TODO: not present in API specification!
    whats_new_badge: boolean;   // If the what's new notifications should be shown
    autosave: boolean;          // If the tool should autosave projects
    unit: UnitType;             // Default measurement unit type
    registration_complete: boolean; // Whether the user has completed their registration
    notifications_checked_at: string | null; // The datetime when the user checked the notifications
    referral: string | null;    // The referral code that the user used during their registration
    trial_expires_at: string | null; // The datetime when the user's trial expires
    delete_requested: boolean | null; // TODO: not present in API specification!
    delete_request_notified: number; // TODO: not present in API specification!
    stripe_id: string | null;   // TODO: not present in API specification!
    stripe_ref: unknown | null; // TODO: not present in API specification!
    pm_type: unknown | null;    // TODO: not present in API specification!
    pm_last_four: unknown | null; // TODO: not present in API specification!
    trial_ends_at: string | null; // TODO: not present in API specification!
    settings: string | null;    // TODO: not present in API specification!
    intercom_identity_token?: string; // [resources endpoint only] Helpdesk token
    has_password?: boolean;     // [resources endpoint only] SSO or not (enables an ability to change the email)
    permissions: ApiPermission[]; // TODO: not present in API specification!
    roles: ApiRole[];           // TODO: not present in API specification!
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;
/* eslint-enable @typescript-eslint/no-redundant-type-constituents */
export type ApiUserData = Flatten<Exclude<ApiUser, ApiError>['data']>;

export type JobFolder = {
  id: number;
  jobsCount: number;
  name: string;
  userId: number;
};

export type ApiJobFolder = {
  data: {
    id: number;
    jobsCount: number;
    name: string;
    userId: number;
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiJobFolderData = Flatten<Exclude<ApiJobFolder, ApiError>['data']>;

export const userRevivers: Revivers<ApiUser, User> = {
  organisation_id: undefined,
  hide_messages: undefined,
  phone: undefined,
  profession: undefined,
  country: undefined,
  city: undefined,
  address: undefined,
  tips: undefined,
  tags: undefined,
  newsletter_consent: undefined,
  registration_complete: undefined,
  notifications_checked_at: undefined,
  referral: undefined,
  delete_requested: undefined,
  delete_request_notified: undefined,
  stripe_ref: undefined,
  pm_type: undefined,
  pm_last_four: undefined,
  trial_ends_at: undefined,
  settings: undefined,
  permissions: undefined,
  roles: undefined,

  trialExpiresAt: (data: ApiUserData) => data.trial_expires_at ?? undefined,
  stripeId: (data: ApiUserData) => data.stripe_id ?? undefined,
};

type UpdateUserParams = {
  whatsNewBadge?: boolean;
  videoNotifications?: boolean;
  autosave?: boolean;
  interfaceLanguage?: string;
  languageCode?: string;
  unit?: UnitType;
  password?: string;
  email?: string;
};

export async function getUser(): Promise<User> {
  const path = `/v1/users/${myUser}`;
  const options = { revivers: userRevivers };

  return request<ApiUser, User>(path, null, null, options);
}

export async function updateUser(
  userId: number | typeof myUser,
  fields: RequireAtLeastOne<UpdateUserParams>,
): Promise<Record<string, never>> {
  const path = `/v1/users/${userId}`;
  const body = toApiType(fields);

  return request<ApiCommon, Record<string, never>>(path, body, null, { method: 'PATCH' });
}

export async function deleteUser(userId: number | typeof myUser): Promise<Record<string, never>> {
  const path = `/v1/users/${userId}`;

  return request<ApiCommon, Record<string, never>>(path, null, null, { method: 'DELETE' });
}

export async function listUserFontFamilies(): Promise<FontFamily[]> {
  const pathname = `/v1/users/${myUser}/font-families`;
  const query = getSearchParams({ sort: 'order' });
  const path = `${pathname}?${query}`;
  const options = { revivers: fontFamilyRevivers };

  type ApiFontFamilyArray = {
    data: ApiFontFamilyData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiFontFamilyArray, FontFamily>(path, null, defaultListHeader, options);
}

export async function listUserHatchingSvgs(): Promise<Svg[]> {
  const pathname = `/v1/users/${myUser}/hatching-svgs`;
  const query = getSearchParams({ sort: 'order' });
  const path = `${pathname}?${query}`;
  const options = { revivers: svgRevivers };

  type ApiSvgArray = {
    data: ApiSvgData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiSvgArray, Svg>(path, null, defaultListHeader, options);
}

export async function listUserJobs(
  userId: number | typeof myUser,
  title: string,
  page: number,
  options?: Record<string, unknown>
): Promise<JobSearchResult> {
  return listJobs(`/v1/users/${userId}/jobs`, title, page, options);
}

export async function listUserFolders(): Promise<JobFolder[]> {
  const path = `/v1/users/${myUser}/folders`;

  type ApiJobFolderArray = {
    data: ApiJobFolderData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiJobFolderArray, JobFolder>(path);
}

export async function createUserFolder(name: string): Promise<JobFolder> {
  const path = `/v1/users/${myUser}/folders`;
  const body = { name };

  return request<ApiCommon, JobFolder>(path, body);
}

export async function deleteUserFolder(folderId: number): Promise<Record<string, never>> {
  const path = `/v1/users/${myUser}/folders/${folderId}`;

  return request<ApiCommon, Record<string, never>>(path, null, null, { method: 'DELETE' });
}

export async function renameUserFolder(folderId: number, name: string): Promise<Record<string, never>> {
  const path = `/v1/users/${myUser}/folders/${folderId}`;
  const body = { name };

  return request<ApiCommon, Record<string, never>>(path, body, null, { method: 'PATCH' });
}

export async function listUserLayers(): Promise<Layer[]> {
  const pathname = `/v1/users/${myUser}/layers`;
  const query = getSearchParams({
    search: {
      pangaea_ready: '1',
    },
    sort: 'order',
  });
  const path = `${pathname}?${query}`;
  const options = { revivers: layerRevivers };

  type ApiLayerArray = {
    data: ApiLayerData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiLayerArray, Layer>(path, null, defaultListHeader, options);
}

export async function listUserSvgSets(): Promise<SvgSet[]> {
  const pathname = `/v1/users/${myUser}/svg-sets`;
  const query = getSearchParams({
    search: {
      supports_mapcreator: '1',
      type: '!:sprite',
    },
    sort: 'order',
  });
  const path = `${pathname}?${query}`;
  const options = { revivers: svgSetRevivers };

  type ApiSvgSetArray = {
    data: ApiSvgSetData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiSvgSetArray, SvgSet>(path, null, defaultListHeader, options);
}

export async function findJobShares(jobId: number): Promise<JobShare[]> {
  const pathname = `/v1/users/${myUser}/job-shares`;
  const query = getSearchParams({ search: { job_id: jobId } });
  const path = `${pathname}?${query}`;

  return request<ApiJobShareArray, JobShare>(path);
}

export async function createUserTrial(
  userId: number | typeof myUser,
  data: Record<string, unknown>,
): Promise<Record<string, never>> {
  const pathname = `/v1/users/${userId}/trial`;
  const path = `${pathname}?${deletedNoneParam}`;
  const body = toApiType(data);

  return request<ApiCommon, Record<string, never>>(path, body);
}

export async function sendUserConfirmationEmail(userId: number | typeof myUser): Promise<Record<string, never>> {
  const path = `/v1/users/${userId}/confirm`;

  return request<ApiCommon, Record<string, never>>(path, null, null, { method: 'POST' });
}

type BillingPortal = {
  billingPortalUrl: string;
};
type ApiBillingPortal = {
  data: {
    billing_portal_url: string;
  };
} & Omit<ApiSuccess, 'data'> | ApiError;

export async function getUserBillingPortalUrl(userId: number | typeof myUser): Promise<string> {
  const path = `/v1/users/${userId}/billing-portal-url`;

  return request<ApiBillingPortal, BillingPortal>(path).then(
    ({ billingPortalUrl }) => billingPortalUrl,
  );
}
