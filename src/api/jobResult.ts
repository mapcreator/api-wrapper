import {
  type ApiCommon,
  type ApiCommonData,
  type ApiError,
  type ApiSuccess,
  type Flatten,
  type Revivers,
  deletedNoneParam,
  lastJobRevision,
  request,
} from '../utils.js';

export type JobResult = {
  jobRevisionId: number;
  revision: number;
  status: string;
};

export type ApiJobResult = {
  data: {
    job_revision_id: number;    // The id of the job revision
    revision: number;           // The revision number of the job revision
    mapstyle_id: number;        // The id of the map style
    interface_version: string;  // The version of the interface (searchable, sortable)
    fix: string;                //
    status: string;             // The status of the job result (searchable, sortable)
    fail_reason: string;        // A human-readable reason of why the map failed to process
    process_start: string;      // The datetime when the job process started (searchable, sortable)
    process_end: string;        // The datetime when the job process ended (searchable, sortable)
    dealt_with: boolean;        // Checks if the Support fixed an issue (searchable, sortable)
    bought: boolean;            // Checks if the result is bought or not (searchable, sortable)
    contract_id: number;        // The id of the contract that was active when the map was created
    callback: string;           // URL to another API which can finish a map (searchable, sortable)
    last_downloaded: string;    // The last time the archive was downloaded (searchable, sortable)
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiJobResultData = Flatten<Exclude<ApiJobResult, ApiError>['data']>;

export const jobResultRevivers: Revivers<ApiJobResult, JobResult> = {
  mapstyle_id: undefined,
  interface_version: undefined,
  fix: undefined,
  fail_reason: undefined,
  process_start: undefined,
  process_end: undefined,
  dealt_with: undefined,
  bought: undefined,
  contract_id: undefined,
  callback: undefined,
  last_downloaded: undefined,
};

export async function uploadJobResultPreview(
  jobId: number,
  blob: Blob,
): Promise<Record<string, never>> {
  const pathname = `/v1/jobs/${jobId}/revisions/${lastJobRevision}/result/preview`;
  const path = `${pathname}?${deletedNoneParam}`;
  const body = new FormData();

  body.append('preview', blob);

  return request<ApiCommon, Record<string, never>>(path, body);
}

export type JobResultHostedImage = {
  url: string;
  message: string;
};

export type ApiJobResultHostedImage = {
  data: {
    url: string;
    message: string;
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiJobResultHostedImageData = Flatten<Exclude<ApiJobResultHostedImage, ApiError>['data']>;

export async function uploadJobResultHostedImage(
  jobId: number,
  blob: Blob,
  fileName: string,
  fileType: 'jpg' | 'png' | 'svg',
): Promise<JobResultHostedImage> {
  const pathname = `/v1/jobs/${jobId}/revisions/${lastJobRevision}/result/hosted-${fileType}`;
  const path = `${pathname}?${deletedNoneParam}`;
  const body = new FormData();

  body.append(fileType, blob);
  body.append('filename', fileName);

  return request<ApiJobResultHostedImage, JobResultHostedImage>(path, body);
}
