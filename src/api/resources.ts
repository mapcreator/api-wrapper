import { type ApiOrganisationData, type Organisation, organisationRevivers } from './organisation.js';
import { type ApiDimensionSetData, type DimensionSet, dimensionSetRevivers } from './dimensionSet.js';
import { type ApiMapstyleSetData, type MapstyleSet, mapstyleSetRevivers } from './mapstyleSet.js';
import { type ApiDimensionData, type Dimension, dimensionRevivers } from './dimension.js';
import { type ApiFeatureData, type Feature, featureRevivers } from './feature.js';
import { type ApiJobTypeData, type JobType, jobTypeRevivers } from './jobType.js';
import { type ApiMessageData, type Message, messageRevivers } from './message.js';
import { type ApiSvg, type ApiSvgData, type Svg, svgRevivers } from './svg.js';
import { type ApiSvgSetData, type SvgSet, svgSetRevivers } from './svgSet.js';
import { type ApiLayerData, type Layer, layerRevivers } from './layer.js';
import { type ApiColorData, type Color, colorRevivers } from './color.js';
import { type ApiUserData, type User, userRevivers } from './user.js';
import type { CamelCasedProperties } from 'type-fest';
import {
  type ApiCommon,
  type ApiCommonData,
  type ApiError,
  type ApiSuccess,
  type Revivers,
  getContext,
  myUser,
  processData,
  request,
} from '../utils.js';

export type CustomFeature =
  | 'custom_dpi'
  | 'disable_company_maps'
  | 'eps_plus_log'
  | 'filename_is_job_title'
  | 'iframe_sandbox_attributes'
  | 'keep_layer_visibility'
  | 'no_copyright'
  | 'only_shared_company_maps'
  | 'pdf_output'
  | 'show_frelex_links'
  | 'svg_output'
  | 'svg-georef'
  | 'tiff_output'
  | 'video_export'
  | 'web_output'
  | 'web_download'
  | 'new_single_and_grouped_areas_menu'
  | 'export_templates'
  | 'hosted_svg_output'
  | 'hosted_png_output'
  | 'hosted_jpg_output';

export interface Resources {
  organisation: Organisation;
  user: User;
  features: CustomFeature[];
  colors: Color[];
  svgs: Svg[];
  dimensions: Dimension[];
  dimensionSets: DimensionSet[];
  mapstyleSets: MapstyleSet[];
  jobTypes: JobType[];
  messages: Message[];
  svgSets: SvgSet[];
  layers: Layer[];
}

type ApiResources = {
  data: {
    user: ApiUserData;
    organisation: ApiOrganisationData;
    features: ApiFeatureData[];
    colors: ApiColorData[];
    svgs: ApiSvgData[];
    messages: {
      read: ApiMessageData[];
      unread: ApiMessageData[];
    };
    dimensions: ApiDimensionData[];
    dimension_sets: ApiDimensionSetData[];
    mapstyle_sets: ApiMapstyleSetData[];
    hatching_svgs?: ApiSvgData[];
    job_types: ApiJobTypeData[];
    svg_sets: ApiSvgSetData[];
    layers: ApiLayerData[];
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiResourcesData = Exclude<ApiResources, ApiError>['data'];

export type FillPattern = {
  name: string;
  string: string;
};

export async function loadResources(): Promise<Resources> {
  const path = `/v1/users/${myUser}/resources`;
  const raw = await request<ApiResources, CamelCasedProperties<ApiResourcesData>>(path);

  const { user, organisation, messages: { read, unread } } = raw;
  const readMessages = read.map(processData, getContext(messageRevivers)) as Message[];
  const unreadMessages = unread.map(processData, getContext(messageRevivers)) as Message[];

  readMessages.forEach(message => (message.read = true));

  const allMessages = [...readMessages, ...unreadMessages];

  allMessages.sort((m1, m2) => m2.date.localeCompare(m1.date));

  const mcSvgRevivers: Revivers<ApiSvg, Svg> = {
    ...svgRevivers,
    name: (data: ApiSvgData) => `mc-image-${data.name}`,
  };

  return {
    // @ts-expect-error TS2345
    user: processData.call(getContext(userRevivers), user) as User,
    // @ts-expect-error TS2345
    organisation: processData.call(getContext(organisationRevivers), organisation) as Organisation,

    features: (raw.features.map(processData, getContext(featureRevivers)) as Feature[]).map(
      feature => feature.name,
    ) as CustomFeature[],
    colors: raw.colors.map(processData, getContext(colorRevivers)) as Color[],
    svgs: raw.svgs.map(processData, getContext(mcSvgRevivers)) as Svg[],
    dimensions: raw.dimensions.map(processData, getContext(dimensionRevivers)) as Dimension[],
    dimensionSets: raw.dimensionSets.map(processData, getContext(dimensionSetRevivers)) as DimensionSet[],
    mapstyleSets: raw.mapstyleSets.map(processData, getContext(mapstyleSetRevivers)) as MapstyleSet[],
    jobTypes: raw.jobTypes.map(processData, getContext(jobTypeRevivers)) as JobType[],
    messages: allMessages,
    layers: (raw.layers?.map(processData, getContext(layerRevivers)) ?? []) as Layer[],
    svgSets: (raw.svgSets?.map(processData, getContext(svgSetRevivers)) ?? []) as SvgSet[],
  };
}

export async function getCountries(): Promise<string[]> {
  return request<ApiCommon, string, true>(`/v1/resources/countries`);
}

export async function getFunctions(): Promise<string[]> {
  return request<ApiCommon, string, true>(`/v1/resources/functions`);
}

export async function getIndustries(): Promise<string[]> {
  return request<ApiCommon, string, true>(`/v1/resources/industries`);
}
