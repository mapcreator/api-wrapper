import {
  type ApiCommonData,
  type ApiError,
  type ApiSuccess,
  type Flatten,
  type Revivers,
  defaultListHeader,
  getSearchParams,
  request,
} from '../utils.js';

export type LayerFaq = {
  id: number;
  name: string;
  description: string;
  mediaUrl: string;
  readMoreUrl: string;
  tags: string[];
  layerId: number;
  order?: number;
};

export type ApiLayerFaq = {
  data: {
    id: number;                 // The id of the layer faq (sortable)
    name: string;               // The name of this layer faq (searchable, sortable)
    description: string;        // The description of the layer faq (searchable)
    media_url: string;          // The image for the faq (searchable)
    read_more_url: string;      // A link to a page with more information (searchable)
    tags: string[];             // A list of tags to search in (searchable)
    layer_id: number;           // TODO: not present in API specification!
    order?: number;             // TODO: not present in API specification!
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiLayerFaqData = Flatten<Exclude<ApiLayerFaq, ApiError>['data']>;

export const layerFaqRevivers: Revivers<ApiLayerFaq, LayerFaq> = {
  order: undefined,
};

export async function listLayerFaqs(): Promise<LayerFaq[]> {
  const pathname = `/v1/layer-faqs`;
  const query = getSearchParams({ sort: 'order,name' });
  const path = `${pathname}?${query}`;
  const options = { revivers: layerFaqRevivers };

  type ApiLayerFaqArray = {
    data: ApiLayerFaqData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiLayerFaqArray, LayerFaq>(path, null, defaultListHeader, options);
}
