import { type ApiSvgData, type Svg, svgRevivers } from './svg.js';
import {
  type ApiCommonData,
  type ApiError,
  type ApiSuccess,
  type Flatten,
  type Revivers,
  defaultListHeader,
  getSearchParams,
  request,
} from '../utils.js';

export type SvgSet = {
  id: number;
  name: string;
  type: string;
};

export type ApiSvgSet = {
  data: {
    id: number;                 // The id of the set (searchable, sortable)
    name: string;               // The name of the set (searchable, sortable)
    type: string;               // The type of the svg set (searchable, sortable)
    supports_interactive: boolean; // If the set is interactive (searchable, sortable)
    supports_mapcreator: boolean; // If this set can be used on Mapcreator (searchable, sortable)
    supports_v3: boolean;       // If this set can be used on v3 (searchable, sortable)
    order?: number;             // TODO: not present in API specification!
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiSvgSetData = Flatten<Exclude<ApiSvgSet, ApiError>['data']>;

export const svgSetRevivers: Revivers<ApiSvgSet, SvgSet> = {
  supports_interactive: undefined,
  supports_mapcreator: undefined,
  supports_v3: undefined,
  order: undefined,
};

export async function listSvgSetSvgs(svgSetId: number): Promise<Svg[]> {
  const pathname = `/v1/svgs/sets/${svgSetId}/items`;
  const query = getSearchParams({
    search: {
      supports_mapcreator: '1',
    },
    sort: 'order,id',
  });
  const path = `${pathname}?${query}`;
  const options = { revivers: svgRevivers };

  type ApiSvgArray = {
    data: ApiSvgData[];
  } & Omit<ApiSuccess, 'data'> | ApiError;

  return request<ApiSvgArray, Svg>(path, null, defaultListHeader, options);
}
