import type { ApiCommonData, ApiError, ApiSuccess, Flatten, Revivers } from '../utils.js';

export type MapstyleSet = {
  id: number;
  name: string;
  supportsSvg: boolean;
};

export type ApiMapstyleSet = {
  data: {
    id: number;                 // The id of the mapstyle set (sortable)
    name: string;               // The name of the mapstyle set (searchable, sortable)
    description: string;        // The description of the set (searchable, sortable)
    supports_svg: boolean;      // If the set supports svg (searchable, sortable)
    job_type_id: number;        // The id of the job type (searchable)
    preview_lat: number;        // The latitude for the preview image
    preview_lon: number;        // The longitude for the preview image
    preview_language_code: string; // The language code of the preview
    world_cache: string | null; // The name of the world cache (sortable)
    world_cache_max_zoom: number | null; // The max zoom level of the world cache (sortable)
    css: string | null;         // Custom CSS for the MapstyleSet (sortable)
    locator_ready: boolean;     // If the set can be used as locator (searchable, sortable)
    show_underlying_elements: boolean; // Shows elements even if behind annotations
    ace_on_osm: boolean;        // Use OSM source as data for ACE maps (searchable, sortable)
    order?: number;             // TODO: not present in API specification!
    custom_settings?: string | null; // TODO: not present in API specification!
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiMapstyleSetData = Flatten<Exclude<ApiMapstyleSet, ApiError>['data']>;

export const mapstyleSetRevivers: Revivers<ApiMapstyleSet, MapstyleSet> = {
  description: undefined,
  job_type_id: undefined,
  preview_lat: undefined,
  preview_lon: undefined,
  preview_language_code: undefined,
  world_cache: undefined,
  world_cache_max_zoom: undefined,
  css: undefined,
  locator_ready: undefined,
  show_underlying_elements: undefined,
  ace_on_osm: undefined,
  order: undefined,
  custom_settings: undefined,
};
