import {
  type ApiCommon,
  type ApiCommonData,
  type ApiError,
  type ApiSuccess,
  type Flatten,
  type Revivers,
  request,
} from '../utils.js';

export type Message = {
  index: number;
  image: string;
  date: string;
  read: boolean;
  type: 'New feature' | 'Announcement';
  video: boolean;
  title: string;
  text: string;
};

type ApiMessageVariant = {
  language_code: string;
  content: string;
  title: string;
} & ApiCommonData;

export type ApiMessage = {
  data: {
    id: number;                 // The message id (sortable)
    media_url: string;          // The media url for this message (searchable, sortable)
    style: string;              // TODO: not present in API specification!
    publish_at: string | null;  // TODO: not present in API specification!
    ends_at: string | null;     // TODO: not present in API specification!
    variants: ApiMessageVariant[];
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiMessageData = Flatten<Exclude<ApiMessage, ApiError>['data']>;

const parseVideo = (url: string, id = getYoutubeVideoId(url)): string =>
  (id ? `https://www.youtube.com/embed/${id}` : url);

export const messageRevivers: Revivers<ApiMessage, Message> = {
  id: undefined,
  media_url: undefined,
  style: undefined,
  publish_at: undefined,
  ends_at: undefined,
  variants: undefined,

  index: (data: ApiMessageData) => data.id,
  image: (data: ApiMessageData) => parseVideo(data.media_url),
  date: (data: ApiMessageData) => data.created_at as string,
  read: () => false,
  type: (data: ApiMessageData) => (data.style === 'regular' ? 'New feature' : 'Announcement'),
  video: (data: ApiMessageData) => data.media_url.includes('www.youtube.com'),
  title: (data: ApiMessageData) => data.variants[0].title,
  text: (data: ApiMessageData) => data.variants[0].content,
};

export async function markMessageAsRead(messageId: number): Promise<Record<string, never>> {
  const path = `/v1/messages/${messageId}/read`;

  return request<ApiCommon, Record<string, never>>(path, null, null, { method: 'PUT' });
}

export function getYoutubeVideoId(url: string): string | undefined {
  url = url.trim();

  // Reference prefixed with a $ or video id
  if (/^\$?[\w\-_]+$/.test(url)) {
    return url;
  }

  const regex =
    /(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)&?/;

  return (regex.exec(url) || [])[1];
}
