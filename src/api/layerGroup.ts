import type { Layer } from './layer.js';
import {
  type ApiCommonData,
  type ApiError,
  type ApiSuccess,
  type Flatten,
  getSearchParams,
  request,
} from '../utils.js';

export type LayerGroup = {
  id: number;
  name: string;
  layers: Layer[];
};

export type ApiLayerGroup = {
  data: {
    id: number;                 // The id of the layer group (sortable)
    name: string;               // The name of the layer group (searchable, sortable)
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiLayerGroupData = Flatten<Exclude<ApiLayerGroup, ApiError>['data']>;

export async function listLayerGroups(layers: Layer[]): Promise<LayerGroup[]> {
  const pathname = `/v1/layer-groups/for-layer-ids`;
  const query = getSearchParams({ layer_ids: layers.map(layer => layer.id) });
  const path = `${pathname}?${query}`;

  type ApiLayerGroupArray = {
    data: Array<ApiLayerGroupData & { layers: Array<{ id: number }> }>;
  } & Omit<ApiSuccess, 'data'> | ApiError;

  const layerGroups = await request<ApiLayerGroupArray, LayerGroup>(path);

  // Arrange layers in an object by id for fast lookups
  const layerMap = new Map(layers.map(layer => [layer.id, layer]));

  // Create a set of all layer ids that belong to a group
  const groupedLayerIds = new Set<number>();

  for (const layerGroup of layerGroups) {
    // Replace layers from the API response with their full representation
    layerGroup.layers = layerGroup.layers
      .map(groupLayer => layerMap.get(groupLayer.id))
      .filter((layer): layer is Layer => layer !== undefined);

    // Remember that these layers belong to a group
    for (const layer of layerGroup.layers) {
      groupedLayerIds.add(layer.id);
    }
  }

  // Get a list of layers that don’t belong to any group
  const miscLayers = layers.filter(layer => !groupedLayerIds.has(layer.id));

  // Sort it alphabetically
  miscLayers.sort((a, b) => a.name.localeCompare(b.name));

  // Put them all in a separate group
  const miscLayerGroup: LayerGroup = {
    id: 999,
    layers: miscLayers,
    name: 'Other layers',
  };

  return [...layerGroups, miscLayerGroup];
}
