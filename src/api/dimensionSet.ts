import type { ApiCommonData, ApiError, ApiSuccess, Flatten, Revivers } from '../utils.js';

export type DimensionSet = {
  id: number;
  name: string;
};

export type ApiDimensionSet = {
  data: {
    id: number;                 // The dimension set id (sortable)
    name: string;               // The dimension set name (searchable, sortable)
    order?: number;             // TODO: not present in API specification!
  } & ApiCommonData;
} & Omit<ApiSuccess, 'data'> | ApiError;

export type ApiDimensionSetData = Flatten<Exclude<ApiDimensionSet, ApiError>['data']>;

export const dimensionSetRevivers: Revivers<ApiDimensionSet, DimensionSet> = {
  order: undefined,
};
