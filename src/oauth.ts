export let apiHost = '';

export let token: {
  type: string;
  token: string;
  expires: Date;

  toString: () => string;
} | null = null;

let apiClientId = '';
let callbackUrl = '';
let oauthScopes = ['*'];

const anchorParams = ['access_token', 'token_type', 'expires_in', 'state'];

const storagePrefix = '_m4n_';
const statePrefix = 'oauth_state_';
const storageName = 'api_token';

const dummyTokenExpires = new Date('2100-01-01T01:00:00');

interface AnchorToken {
  access_token: string;
  token_type: string;
  expires_in: string;
  state: string;
}

const titleCase = (str: unknown): string => String(str).toLowerCase().replace(/\b\w/g, c => c.toUpperCase());

/**
 * Setup internal structures to use dummy authentication flow
 *
 * @param {string} apiUrl - Full API URL
 * @param {string} oauthToken - OAuth Token
 */
export function initDummyFlow(apiUrl: string, oauthToken: string): void {
  const parts = oauthToken.includes(' ') ? oauthToken.split(' ', 2) : ['Bearer', oauthToken];

  apiHost = apiUrl.replace(/\/+$/, '');
  token = {
    type: titleCase(parts[0]),
    token: parts[1],
    expires: dummyTokenExpires,

    toString(): string {
      return `${this.type} ${this.token}`;
    },
  };
}

/**
 * Setup internal structures to use implicit authentication flow
 *
 * @param {string} apiUrl - Full API URL
 * @param {string} clientId - OAuth client id
 * @param {string} [redirectUrl] - Callback URL
 * @param {string[]} [scopes] - A list of required scopes
 */
export function initImplicitFlow(apiUrl: string, clientId: string, redirectUrl = '', scopes = ['*']): void {
  apiHost = apiUrl.replace(/\/+$/, '');

  apiClientId = String(clientId);
  callbackUrl = String(redirectUrl || window.location.href.split('#')[0]);
  oauthScopes = scopes;

  {
    const key = `${storagePrefix}${storageName}`;
    const data = window.localStorage.getItem(key);

    if (data) {
      try {
        const obj = JSON.parse(data) as { type?: unknown; token?: unknown; expires?: unknown };

        if (
          typeof obj.type === 'string' &&
          typeof obj.token === 'string' &&
          typeof obj.expires === 'string' &&
          new Date(obj.expires) > new Date()
        ) {
          token = {
            type: titleCase(obj.type),
            token: obj.token,
            expires: new Date(obj.expires),

            toString(): string {
              return `${this.type} ${this.token}`;
            },
          };
        } else {
          window.localStorage.removeItem(key);
        }
      } catch (e) {
        /* */
      }
    }
  }

  {
    const obj = getAnchorToken();

    if (isAnchorToken(obj)) {
      // We'll not go there if anchor contains error and/or message
      // This means that anchor parameters will be preserved for the next processing
      cleanAnchorParams();

      const expires = new Date(Date.now() + Number(obj.expires_in) * 1000);

      if (isValidState(obj.state) && expires > new Date()) {
        token = {
          type: titleCase(obj.token_type),
          token: obj.access_token,
          expires,

          toString(): string {
            return `${this.type} ${this.token}`;
          },
        };

        const key = `${storagePrefix}${storageName}`;
        const data = { type: token.type, token: token.token, expires: expires.toUTCString() };

        window.localStorage.setItem(key, JSON.stringify(data));
      } else {
        // TODO: add some logic to handle this
        // throw Error('Invalid state in url');
      }
    }
  }

  if (authenticated()) {
    const href = sessionStorage.getItem('redirect-url');

    if (href) {
      sessionStorage.removeItem('redirect-url');
      window.history.replaceState(null, document.title, href);
    }
  }
}

export async function authenticate(): Promise<string> | never {
  return new Promise(() => {
    if (anchorContainsError()) {
      console.error(getError());
      cleanAnchorParams();
    }

    forget();

    sessionStorage.setItem('redirect-url', window.location.href);
    window.location.assign(buildRedirectUrl());
  });
}

export function authenticated(): boolean {
  return token != null && token.expires > new Date() && (
    token.expires.valueOf() === dummyTokenExpires.valueOf() ||
    !!window.localStorage.getItem(`${storagePrefix}${storageName}`)
  );
}

export async function logout(): Promise<void> {
  if (token) {
    await fetch(`${apiHost}/oauth/logout`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: token.toString(),
      },
    });
  }

  forget();
}

function forget(): void {
  for (let i = 0; i < window.localStorage.length; ++i) {
    const key = window.localStorage.key(i);

    if (key?.startsWith(storagePrefix)) {
      window.localStorage.removeItem(key);
    }
  }

  token = null;
}

function buildRedirectUrl(): string {
  const queryParams = new URLSearchParams({
    client_id: apiClientId,
    redirect_uri: callbackUrl,
    response_type: 'token',
    scope: oauthScopes.join(' '),
    state: generateState(),
  });

  return `${apiHost}/oauth/authorize?${queryParams}`;
}

function getAnchorQuery(): string {
  return window.location.hash.replace(/^#\/?/, '');
}

function getAnchorParams(): Record<string, unknown> {
  const query = getAnchorQuery();
  // eslint-disable-next-line @stylistic/padding-line-between-statements,@typescript-eslint/no-unsafe-return
  return Object.fromEntries(query.split('&').map(pair => pair.split('=').map(decodeURIComponent)));
}

function getAnchorToken(): Partial<AnchorToken> {
  const params = getAnchorParams();

  return Object.fromEntries(Object.entries(params).filter(([key]) => anchorParams.includes(key)));
}

function isAnchorToken(anchorToken: Partial<AnchorToken>): anchorToken is AnchorToken {
  const queryKeys = Object.keys(anchorToken);

  return anchorParams.every(key => queryKeys.includes(key));
}

function cleanAnchorParams(): void {
  const query = window.location.hash.replace(/^#\/?/, '');
  const targets = [...anchorParams, 'error', 'message'];
  const newHash = query
    .split('&')
    .filter(pair => !targets.includes(decodeURIComponent(pair.split('=')[0])))
    .join('&');

  if (newHash) {
    window.location.hash = newHash;
  } else {
    const { origin, pathname, search } = window.location;

    window.history.replaceState(null, document.title, `${origin}${pathname}${search}`);
  }
}

function isValidState(state: string): boolean {
  const key = `${storagePrefix}${statePrefix}${state}`;
  const found = window.localStorage.getItem(key) != null;

  if (found) {
    window.localStorage.removeItem(key);
  }

  return found;
}

function anchorContainsError(): boolean {
  return 'error' in getAnchorParams();
}

function generateState(): string {
  // @ts-expect-error TS2365
  // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
  const state = (([1e7] + -1e3 + -4e3 + -8e3 + -1e11) as string).replace(
    /[018]/g, // @ts-expect-error TS2362
    c => (c ^ ((Math.random() * 256) & (0x0f >>> (c >>> 2)))).toString(16),
  );
  const key = `${storagePrefix}${statePrefix}${state}`;

  window.localStorage.setItem(key, `${Date.now()}`);

  return state;
}

class OAuthError extends Error {
  error: string;

  constructor(message: string, error: unknown) {
    super(message);

    this.error = String(error);
  }

  toString(): string {
    let error = this.error;

    if (error.includes('_')) {
      error = error.replace('_', ' ').replace(/^./, c => c.toUpperCase());
    }

    return this.message ? `${error}: ${this.message}` : error;
  }
}

function getError(): OAuthError {
  const params = getAnchorParams();

  return params.message
    ? new OAuthError(params.message as string, params.error)
    : new OAuthError(titleCase(params.error), params.error);
}

/**
 * Our goal is to support even obsolete platforms (ES2017+ / Node.js 8.10+).
 * This is a small polyfill for possibly missing method used in our codebase.
 */
if (!Object.fromEntries) { // eslint-disable-next-line arrow-body-style
  Object.fromEntries = <T = never>(entries: Iterable<readonly [string | number, T]>): { [k: string]: T } => {
    return Array.from(entries).reduce<{ [k: string]: T }>(
      (object, entry) => {
        if (!Array.isArray(entry)) {
          throw new TypeError(`Iterator value ${entry as unknown as string} is not an entry object.`);
        }
        object[`${entry[0]}`] = entry[1];

        return object;
      }, {}
    );
  };
}
