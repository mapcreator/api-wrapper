### Used type system

We use type declarations for both the data coming over the wire and the data used by the application. In general, these types differ only in the presence of additional fields in the data arriving over the network (like `created_at`), and used naming convention. Data over the network uses the so-called snake case.

All in all, we can use TypeScript's native mechanisms to convert one type to another and fully define just one type:

```typescript
import type { CamelCasedProperties } from 'type-fest';

type ApiType =
  | ({
      data: {
        prop: unknown;
      } & ApiCommonData;
    } & Omit<ApiSuccess, 'data'>)
  | ApiError;

type AppType = CamelCasedProperties<Omit<Exclude<ApiType, ApiError>['data'], keyof ApiCommonData>>;
```

or in reverse order:

```typescript
import type { SnakeCasedProperties } from 'type-fest';

type AppType = {
  prop: unknown;
};

type ApiType =
  | ({
      data: SnakeCasedProperties<AppType> & ApiCommonData;
    } & Omit<ApiSuccess, 'data'>)
  | ApiError;
```

But the decision was made not to do this, since it may be more difficult for the developer to make the conversion in their head than to see it in front of their eyes.

### Using a `request()`

The function has the following signature:

```typescript
async function request<I extends ApiCommon, O extends Record<string, unknown> | string>(
  path: string,
  body?: XMLHttpRequestBodyInit | Record<string | number, unknown> | null,
  extraHeaders?: Record<string, string> | null,
  extraOptions?: ExtraOptions<I, O>,
): Promise<O | O[]> { /* ... */ }
```

Let's take it step by step.

`I extends ApiCommon` - represents the type of data we receive over the network.

`O extends Record<string, unknown> | string` - represents the data type that will be used in the application.

Ideally you should describe and convey both types. This will help to check the data types in the arguments passed.
See current data types for an example.

`path: string` - the path to the resource, must include the API version, but must not include the schema or authority.
Example: `/v1/jobs/12345`

`body?: XMLHttpRequestBodyInit | Record<string | number, unknown> | null` - any meaningful body type. In general,
the presence of an JSON object is assumed (or the absence of one for methods that only request data), but you can
also pass `Blob`, `FormData`, `URLSearchParams` or just `ArrayBuffer`. The required content type will be added to
the headers automatically.

`extraHeaders?: Record<string, string> | null` - the object with additional headers.

`extraOptions?: ExtraOptions<I, O>` - where `ExtraOptions<I, O>` is defined like this:

```typescript
interface ExtraOptions<I extends ApiCommon, O extends Record<string, unknown> | string> {
  method?: 'GET' | 'HEAD' | 'POST' | 'PUT' | 'DELETE' | 'PATCH';
  revivers?: O extends Record<string, unknown> ? Revivers<I, O> : never;
  sendNull?: boolean;
  withMeta?: boolean;
}
```

Most fields are self-explanatory.

`sendNull` can be used if you really want to pass `null` as body content.

`revivers` is used to specify an object that can modify the behavior of the internal handler of data coming over
the network. Let's take a closer look at this moment.

#### Revivers

By default, the `request()` function does the following things with data coming over the network:

- It removes `created_at`, `updated_at`, `deleted_at` fields from the output objects.
- It preserves all the remaining fields but converts their names into camelCase.

When passing an object with revivers you can a couple of things:

- You can list the fields that you want **to exclude** from the result object. To do this, the field must be assigned an
`undefined` value.
- You can **add** new fields or **modify** the type of existing ones. To do this, you need to pass a function as a field
value, which will receive the original object as input.

Example:

```typescript
const jobRevivers: Revivers<ApiJob, Job> = {
  user_id: undefined,
  description: undefined,
  share_token: undefined,
  autosave_preview_path: undefined,
  job_folder_id: undefined,

  jobTypeId: () => 9,
  createdAt: (data: ApiJobData) => data.created_at as string,
  previewPath: (data: ApiJobData) => data.autosave_preview_path ?? undefined,
};
```

`user_id`, `description`, `share_token`, `autosave_preview_path`, `job_folder_id` fields will be excluded from the
result object.

`jobTypeId` will be always **9**.

`createdAt` will be returned (please note that that field is excluded by default)

`previewPath` - some actions will be performed with the source data.
