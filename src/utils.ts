import type { CamelCasedProperties, SnakeCasedProperties } from 'type-fest';
import { apiHost, authenticate, token } from './oauth.js';

export type Flatten<Type> = Type extends Array<infer Item> ? Item : Type;

export type ApiCommonData = {
  created_at: string | null;
  deleted_at: string | null;
  updated_at: string | null;
};

export interface ApiSuccess {
  success: true;
  data?: Record<string, unknown> | Array<Record<string, unknown>> | string | string[];
}

export interface ApiError {
  success: false;
  error: {
    type: string;
    message: string;
    validation_errors?: Record<string, string[]>;
    // schema_errors?: Record<string, unknown>[];
  };
}

export type ApiCommon = ApiSuccess | ApiError;

export const myUser = 'me';
export const myOrganisations = 'mine';
export const lastJobRevision = 'last';

export const defaultListHeader = { 'X-Per-Page': '50' };
export const deletedNoneParam = getSearchParams({ deleted: 'none' });

export const keysToRemove = ['created_at', 'deleted_at', 'updated_at'];
export const staticContext = { keysToRemove: new Set(keysToRemove), keysToAdd: [], revivers: {} };

// class AuthorizationError extends Error {}
export class NetworkError extends Error {}

export class HTTPError extends Error {
  statusCode: number;
  constructor(response: Response) {
    super(response.statusText);
    this.statusCode = response.status;
  }
}

export class APIError extends Error {
  code: string;
  constructor(apiError: ApiError) {
    super(apiError.error.message);
    this.code = apiError.error.type;
  }
}

export function getSearchParams(search: Record<string, unknown>): URLSearchParams {
  return new URLSearchParams(
    Object.entries(search).reduce<string[][]>((arr, [key, value]) => {
      if (Array.isArray(value)) {
        arr.push(...value.map(val => [`${key}[]`, String(val)]));
      } else if (value !== null && typeof value === 'object') {
        const params = getSearchParams(value as Record<string, unknown>);

        arr.push(
          ...Array.from(params.entries()).map(([subKey, val]) => [
            ~subKey.indexOf('[')
              ? `${key}[${subKey.slice(0, subKey.indexOf('['))}]${subKey.slice(subKey.indexOf('['))}`
              : `${key}[${subKey}]`,
            val,
          ]),
        );
      } else {
        arr.push([key, String(value)]);
      }

      return arr;
    }, []),
  );
}

export type Revivers<
  I extends ApiCommon,
  O extends Record<string, unknown>,
  R = Exclude<Flatten<Exclude<Exclude<I, ApiError>['data'], string | undefined>>, string>,
> = {
  [K in keyof O | keyof R]?: K extends keyof O & keyof R
    ? ((data: R) => O[K]) | undefined
    : K extends Exclude<keyof O, keyof R>
      ? (data: R) => O[K]
      : K extends Exclude<keyof R, keyof O>
        ? undefined
        : never;
};
export interface ExtraOptions<I extends ApiCommon, O extends Record<string, unknown> | string> {
  method?: 'GET' | 'HEAD' | 'POST' | 'PUT' | 'DELETE' | 'PATCH'; // 'CONNECT' | 'OPTIONS' | 'TRACE'
  revivers?: O extends Record<string, unknown> ? Revivers<I, O> : never;
  sendNull?: boolean;
  withMeta?: boolean;
}

export class APIMeta<
  I extends ApiCommon,
  O extends Record<string, unknown> | string,
  A = Exclude<I, ApiError>['data'] extends unknown[] ? true : false,
> extends Error {
  data: A extends true ? O[] : O;
  status: number;
  headers: Headers;

  constructor(data: A extends true ? O[] : O, headers: Headers, status: number) {
    super();

    this.data = data;
    this.status = status;
    this.headers = headers;
  }
}

export async function request<
  I extends ApiCommon,
  O extends Record<string, unknown> | string,
  A = Exclude<I, ApiError>['data'] extends unknown[] ? true : false,
>(
  path: string,
  body?: XMLHttpRequestBodyInit | Record<string | number, unknown> | null,
  extraHeaders?: Record<string, string> | null,
  extraOptions?: ExtraOptions<I, O>,
): Promise<A extends true ? O[] : O> {
  type R = A extends true ? O[] : O;

  const href = `${apiHost}${path}`;
  const init = getRequestInit(body, extraHeaders, extraOptions);

  const response = await fetch(href, init).catch((error: Error) => {
    throw new NetworkError(error?.message ?? error);
  });

  if (response.ok) {
    const json = (await response.json().catch(() => ({
      success: false,
      error: { type: 'SyntaxError', message: 'Malformed JSON response' },
    }))) as I;

    if (json.success) {
      const { data } = json;

      const withMeta = extraOptions?.withMeta;
      const revivers = extraOptions?.revivers;
      const keys = revivers ? Object.keys(revivers) : [];

      const context =
        revivers && keys.length
          ? getContext(revivers as Revivers<I, Record<string, unknown>>, keys)
          : staticContext;

      let result: R;

      if (Array.isArray(data)) {
        let moreData = [] as unknown as R;
        const headers = response.headers;
        // @ts-expect-error TS2362, get valid number or 0
        const total = headers.get('X-Paginate-Total') | 0;

        if (!withMeta && ((data.length && total > data.length) || response.status === 206)) {
          // @ts-expect-error TS2362, get valid number or 0
          const offset = `${(headers.get('X-Paginate-Offset') | 0) + data.length}`;

          moreData = await request<I, O, A>(
            path, body, { ...extraHeaders, 'X-Offset': offset }, extraOptions,
          );
        }

        if (data.length) {
          result = (
            typeof data[0] !== 'string'
              ? (data as Array<Record<string, unknown>>).map(processData, context)
              : data
          ).concat(moreData) as R;
        } else {
          result = moreData;
        }
      } else {
        if (data !== undefined) {
          // @ts-expect-error TS2345, TODO: fix type error for the `context` object
          result = (typeof data !== 'string' ? processData.call(context, data) : data) as R;
        } else {
          result = {} as R;
        }
      }

      if (withMeta) {
        throw new APIMeta<I, O, A>(result, response.headers, response.status);
      }

      return result;
    } else {
      throw new APIError(json);
    }
  } else {
    // TODO: parse and process all typical errors
    // eslint-disable-next-line default-case
    switch (response.status) {
      case 401:
        await authenticate();
        break; // NO-OP
      case 403:
      case 404:
      case 406:
        throw new APIError(
          (await response.json().catch(() => ({
            success: false,
            error: { type: 'HttpException', message: response.statusText },
          }))) as ApiError,
        );
      case 429:
        await new Promise(
          (
            resolve, // @ts-expect-error TS2531
          ) => { setTimeout(resolve, response.headers.get('X-RateLimit-Reset') * 1000 || 500) },
        );

        return request<I, O, A>(path, body, extraHeaders, extraOptions);
    }

    throw new HTTPError(response);
  }
}

function getRequestInit<I extends ApiCommon, O extends Record<string, unknown>>(
  body?: XMLHttpRequestBodyInit | Record<string | number, unknown> | null,
  extraHeaders?: Record<string, string> | null,
  extraOptions?: ExtraOptions<I, O>,
): RequestInit {
  const authorization = token ? { Authorization: token.toString() } : null;
  let contentType = null as { 'Content-Type': string } | null;

  if (body !== undefined) {
    switch (true) {
      case typeof body === 'string':
        contentType = { 'Content-Type': 'text/plain' };
        break;
      case body instanceof FormData: // Autofilled with 'multipart/form-data'
      case body instanceof URLSearchParams: // Autofilled with 'application/x-www-form-urlencoded'
        break;
      case body instanceof Blob: // File is instance of Blob too
        contentType = { 'Content-Type': body.type || 'application/octet-stream' };
        break;
      case body instanceof ArrayBuffer:
      case ArrayBuffer.isView(body):
        contentType = { 'Content-Type': 'application/octet-stream' };
        break;
      case body === null && !extraOptions?.sendNull:
        break;
      default:
        contentType = { 'Content-Type': 'application/json' };
        body = JSON.stringify(body);
    }
  }

  const headers = { Accept: 'application/json', ...authorization, ...contentType, ...extraHeaders };
  const method = extraOptions?.method ?? (body != null ? 'POST' : 'GET'); // don't touch `!=` please

  return { body, headers, method } as RequestInit;
}

interface Context<I extends ApiCommon, O extends Record<string, unknown>> {
  keysToRemove: Set<string>;
  keysToAdd: string[];
  revivers: Revivers<I, O>;
}

export function getContext<I extends ApiCommon, O extends Record<string, unknown>>(
  revivers: Revivers<I, O>,
  keys = Object.keys(revivers),
): Context<I, O> {
  return {
    keysToRemove: new Set(keys.filter(isUndefined, revivers).concat(keysToRemove)),
    keysToAdd: keys.filter(isReviver, revivers),
    revivers,
  };
}

export function processData<I extends ApiCommon, O extends Record<string, unknown>>(
  this: Context<I, O>,
  data: Exclude<Flatten<Exclude<Exclude<I, ApiError>['data'], string | undefined>>, string>,
): O { /* eslint-disable @typescript-eslint/prefer-for-of */
  const result = {} as Record<string, unknown>;

  const keys = Object.keys(data).filter(isPreserved, this.keysToRemove);

  for (let i = 0; i < keys.length; ++i) {
    result[toCamelCase(keys[i])] = data[keys[i]];
  }

  if (this.keysToAdd.length) {
    const keys = this.keysToAdd;

    for (let i = 0; i < keys.length; ++i) {
      result[keys[i]] = this.revivers[keys[i]]!(data);
    }
  }

  return result as O;
} /* eslint-enable @typescript-eslint/prefer-for-of */

export function toApiType<T extends Record<string, unknown>>(body: T): SnakeCasedProperties<T> {
  return Object.fromEntries(
    Object.entries(body).map(([k, v]) => [toSnakeCase(k), v]),
  ) as SnakeCasedProperties<T>;
}

export function toAppType<T extends Record<string, unknown>>(body: T): CamelCasedProperties<T> {
  return Object.fromEntries(
    Object.entries(body).map(([k, v]) => [toCamelCase(k), v]),
  ) as CamelCasedProperties<T>;
}

export function ensureArray<T extends Record<string, unknown>>(data: T | T[]): T[] {
  return Array.isArray(data) ? data : [data];
}

function toCamelCase(key: string): string {
  return key.replace(/_+(.)/g, (_, c: string) => c.toUpperCase());
}

function toSnakeCase(key: string): string {
  return key.replace(/[A-Z]/g, '_$&').toLowerCase();
}

function isUndefined(this: Record<string, unknown>, key: string): boolean {
  return this[key] === undefined;
}

function isReviver(this: Record<string, unknown>, key: string): boolean {
  return this[key] !== undefined;
}

function isPreserved(this: Set<string>, key: string): boolean {
  return !this.has(key);
}
