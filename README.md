# Mapcreator Javascript API

The Mapcreator API is a powerful mapping service built for our front-end applications. This library is released to
provide a painless way of talking to the api using Javascript. See the LICENSE file for licensing information. Api
tokens can be granted through support requests.

## Table of contents

* [Installation](#installation)
* [Quick start](#quick-start)
* [Common usage](#common-usage)
* [Publishing](#publishing)

### Installation

```bash
npm install @mapcreator/api --save
```

### Quick start

```javascript
import { authenticate, initImplicitFlow } from '@mapcreator/api/oauth';

/**
 * You need to initialize the authentication module.
 * This is an important step that should be completed as soon as possible after general application init.
 *
 * @param {string} apiUrl - Full API URL, like 'https://api.mapcreator.io'
 * @param {string} clientId - OAuth client id, issued for particular application
 * @param {string} [redirectUrl] - Callback URL, typically your application URL
 * @param {string[]} [scopes] - A list of required scopes, like ['mapcreator']
 */
initImplicitFlow(
  import.meta.env.VITE_API_HOST,
  import.meta.env.VITE_API_CLIENT_ID,
  import.meta.env.VITE_API_REDIRECT_URL,
  ['mapcreator'],
);

// ...

/**
 * The subsequent command should lead to two scenarios.
 * If the initialization function was able to obtain the token from the local storage or from the address bar,
 * then this command is NO-OP. If the token is not found or has expired, then a redirect will be made to the
 * authentication page (the application will unload), but when it redirects back again (see the parameter above),
 * this method won't do anything and the application will be able to continue its work.
 */
authenticate();
```

### Common usage

The library exports independent methods for accessing the API. The recommended pattern for importing and using methods
looks like this:

```typescript
import { updateJob } from '@mapcreator/api/job';

await updateJob(someJobId, newJobTitle);
```

You can also import methods by specifying only the package name, but this results in the use of a common file with
re-exports and can negatively affect the size of the resulting code if you do not need some parts of the API.

```typescript
import { type LayerFaq, listLayerFaqs } from '@mapcreator/api';

const layerFaqs: LayerFaq = await listLayerFaqs();
```

TypeScript types are also exposed, and you can use them whenever needed.

Please consult the contents of the library. All API access files (they are located in `api` sub-folder and each file
generally describes methods for accessing specific entity) describe both the API type and the data type returned
to the application. We sincerely hope this helps you make your app development easier.

## Publishing
```shell
npm version patch # minor | major
npm publish
```
Until the contents of the package are stabilized, it is suggested to use the scheme with the **alpha** or **beta**
suffixes. And therefore the call to the first command should be replaced by manual modification of the `package.json`
and `package-lock.json` files (will need to change in three places).
